const bluebird = require('bluebird');
var mongoose = require( 'mongoose' );

function createModels() {
	mongoose.Promise = bluebird;
	mongoose.connect( 
		process.env.MONGODB_URI || 'mongodb://localhost:27017/myaccounting2017', 
		{ 
			useMongoClient: true,
		}
	);

	return {
		transaction: mongoose.model( 'Transaction', require( './transaction' ), 'transactions' ),
		fixedTransaction: mongoose.model( 'FixedTransaction', require( './fixed_transaction' ), 'fixed_transactions' ),
		category: mongoose.model( 'Category', require( './category' ), 'categories' ),
		user: mongoose.model( 'User', require( './user' ), 'users' ),
	}
};

module.exports = createModels;