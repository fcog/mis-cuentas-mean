var mongoose = require( 'mongoose' );
var Category = require( './category' );
var utils = require( './../utils/utils' );

var transactionSchema = {
  // for UX (field validations): the order of this schema´s properties must be in the same order as presented in the form from top to bottom (except hidden fields).
  user: { 
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User',
    required: true 
  },  
  type: { 
    type: String,
    enum: ['Expense', 'Income'],
    required: true
  },
  category: Category.categorySchema,  
  title: { 
  	type: String,
    minlength: 2,
    maxlength: 60,
    required: true
  },
  payment_amount: { 
  	type: Number,
    min: 0,
    max: 999999999999999
  },
  payment_type: { 
    type: String,
    enum: ['Debit', 'Credit'] 
  },  
  payment_date: { 
  	type: Date 
  },
  payment_date_limit: { 
  	type: Date 
  },
  fixed: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'FixedTransaction',
  },
  order_date: { 
    type: Date,
    required: true 
  },  
  urgent: Boolean
};

var schema = new mongoose.Schema(transactionSchema, { timestamps: true });

//TODO log the validation errors
//TODO make validation tests
schema.pre('validate', function(next) {
  //console.log(this);
  if ( this.type === undefined ){
    // type is a hidden field
    next(Error('Fatal Error! Error #101. Try refreshing the site.'));
  } 
  else if ( this.user === undefined ){
    // user is a hidden field
    next(Error('Fatal Error! Error #102. Try refreshing the site.'));
  }    
  else if ( this.category.name === undefined ){
    next(Error('The category is required.'));
  }    
  else if ( this.title === undefined ){
    next(Error('The title is required.'));
  }
  // The payment amount is only obligatory when a payment type is chosen
  // Viceversa does not apply because a payment amount can be set for a future payment (ej: from a fixed transaction)
  else if ( this.payment_amount === undefined && this.payment_type !== undefined ) {
    next(Error('The payment amount is required when submitting a payment type.'));
  }
  else if ( ( this.payment_amount === null || this.payment_amount === undefined ) && this.payment_date !== undefined ) {
    next(Error('The payment amount is required when adding a payment date.'));
  }
  else if ( this.payment_type === undefined && this.payment_date !== undefined ) {
    next(Error('The payment type is required when adding a payment date.'));
  }    
  else if ( this.payment_amount !== undefined && this.payment_date === null && this.payment_date_limit === null ) {
    next(Error('A payment date or a payment date limit is required when submitting a payment amount.'));
  } 
  else if ( this.payment_date === null && this.payment_type !== undefined ) {
    next(Error('A payment date is required when submitting a payment type.'));
  }
  else if ( this.payment_date !== null && this.payment_date > new Date() ) {
    next(Error('The payment date can\'t be in the future.'));
  }       
  else {
    next();
  }
});

// schema.virtual('urgent').get(function() {
// 	return typeof this.payment_date_limit != 'undefined' &&
//         !this.payment_amount &&
//         utils.dateDiffInDays(new Date(), this.payment_date_limit) < 4;
// });

schema.virtual('id').get(function() {
  return mongoose.Types.ObjectId(this._id);
});

schema.set('toObject', { virtuals: true });
schema.set('toJSON', { virtuals: true });

module.exports = schema;
module.exports.transactionSchema = transactionSchema;