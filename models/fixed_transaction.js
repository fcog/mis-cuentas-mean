var mongoose = require( 'mongoose' );
var Category = require( './category' );

var fixedTransactionSchema = {
    // for UX (field validations): the order of this schema´s properties must be in the same 
    // order as presented in the form from top to bottom (except hidden fields).
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    },
    type: {
        type: String,
        enum: ['Expense', 'Income']
    },
    category: Category.categorySchema,
    title: {
        type: String,
        minlength: 2,
        maxlength: 60
    },
    payment_amount: {
        type: Number,
        min: 0,
        max: 999999999999999
    },
    repetition: {
        day: {
            type: Number,
            min: 1,
            max: 31,
            required: true
        },
        type: {
            type: String,
            enum: ['Daily', 'Weekly', 'Monthly'],
            required: true
        }
    }
};

var schema = new mongoose.Schema(fixedTransactionSchema, { timestamps: true });

//TODO log the validation errors
//TODO make validation tests
schema.pre('validate', function(next) {
    //console.log(this);
    if ( this.type === undefined ){
        // type is a hidden field
        next(Error('Fatal Error! Error #101. Try refreshing the site.'));
    }
    else if ( this.user === undefined ){
        // user is a hidden field
        next(Error('Fatal Error! Error #102. Try refreshing the site.'));
    }
    else if ( this.category.name === undefined ){
        next(Error('The category is required.'));
    }
    else if ( this.title === undefined ){
        next(Error('The title is required.'));
    }
    else if ( this.payment_amount !== undefined && this.payment_date_limit === null ) {
        next(Error('A payment date limit is required when submitting a payment amount.'));
    }
    else {
        next();
    }
});

schema.virtual('id').get(function() {
    return mongoose.Types.ObjectId(this._id);
});

module.exports = schema;
module.exports.fixedTransactionSchema = fixedTransactionSchema;