var mongoose = require('mongoose');
var findOrCreate = require('mongoose-findorcreate');

var categorySchema = {
  name: { 
		type: String,
	  required: true,
	  minlength: 2,
	  maxlength: 15
	},
	type: {
		type: String,
		required: true,
		enum: ['Expense', 'Income']
	},
	user: {
  	type: mongoose.Schema.Types.ObjectId, 
  	ref: 'User', 
  	required: true 
  }
};

var schema = new mongoose.Schema(categorySchema, { timestamps: true });

schema.virtual('id').get(function() {
	return mongoose.Types.ObjectId(this._id);
});

schema.plugin(findOrCreate);

module.exports = schema;
module.exports.categorySchema = categorySchema;
