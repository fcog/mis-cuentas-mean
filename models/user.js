var mongoose = require( 'mongoose' );

var userSchema = {
  profile: {
    email: {
      type: String,
      required: true,
      lowercase: true
    },
    givenName: {
      type: String
    },
    familyName: {
      type: String
    },
    picture: {
      type: String,
      required: true,
      match: /^http:\/\//i
    }
  },
  data: {
    oauth: { type: String, required: true }
  }
};

schema = new mongoose.Schema( userSchema, { timestamps: true } );

schema.virtual( 'id' ).get( () => mongoose.Types.ObjectId( this._id ) );

module.exports = schema;
module.exports.set( 'toObject', { virtual: true } );
module.exports.set( 'toJSON', { virtual: true } );
