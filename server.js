var express = require( 'express' );
var winston = require( 'winston' );

var Models = require( './models/models' )();
var getConfig = require( './utils/configuration' )();

//require('./migration')( Models.user, Models.fixedTransaction, Models.transaction, Models.category );

//Create log file.
winston.add( winston.transports.File, { filename: 'log.log' } );

var app = express();

//Initialize Authentication.
require( './utils/auth' )( Models.user, app, getConfig );

// Load APIs
app.use( '/api/v1', require( './api/usersApi' )( Models.user ) );
app.use( '/api/v1', require( './api/transactionsApi' )( Models.transaction, Models.category ) );
app.use( '/api/v1', require( './api/fixedTransactionsApi' )( Models.fixedTransaction, Models.transaction, Models.category ) );
app.use( '/api/v1', require( './api/categoriesApi' )( Models.category, Models.transaction ) );
app.use( '/api/v1', require( './api/statisticsApi' )( Models.transaction ) );

//Initialize Cron functions.
require( './utils/cron' )( Models.user, Models.fixedTransaction, Models.transaction );

// Serve up static HTML pages from the file system.
// For instance, '/6-examples/hello-http.html' in
// the browser will show the '../6-examples/hello-http.html' file.
app.use( express.static( './frontend/', { maxAge: 4 * 60 * 60 * 1000 /* 2hrs */ } ) );

// Load server
app.listen( process.env.PORT || 8080, function() {
  console.log( `Listening on server port: ${this.address().port}` );
});
