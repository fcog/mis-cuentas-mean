var assert = require('assert');
var express = require('express');
var status = require('http-status');  
var superagent = require('superagent');
var wagner = require('wagner-core');

var URL_ROOT = 'http://localhost:3002';

var transactions = [
  {
    title: 'Dinner',
    category: { name: 'Food', type: 'Expense' },
    type: 'Expense',
    payment_type: 'Credit',
    payment_amount: 1000,
    payment_date: new Date('2016-06-20'),
    order_date: new Date('2016-06-20'),
    payment_date_limit: new Date('2016-06-22')
  },    
  {
    title: 'Shopping',
    category: { name: 'Personal', type: 'Expense' },
    type: 'Expense',
    payment_type: 'Debit',
    payment_amount: 2000,
    payment_date: new Date('2016-06-15'),
    order_date: new Date('2016-06-15'),
    payment_date_limit: new Date('2016-06-22')
  },
  {
    //urgent to be paid example
    title: 'Energy',
    category: { name: 'Public Services', type: 'Expense' },
    type: 'Expense',
    payment_amount: null,
    order_date: new Date('2016-05-27'),
    payment_date_limit: new Date('2016-05-27'),
    urgent: true
  },
  {
    title: 'Ice Cream',
    category: { name: 'Food', type: 'Expense' },
    type: 'Expense',
    payment_type: 'Debit',    
    payment_amount: 100,
    payment_date: new Date('2016-05-11'),
    order_date: new Date('2016-05-11'),
    payment_date_limit: new Date('2016-06-15')
  },
  {
    title: 'Salary',
    category: { name: 'Salary', type: 'Income' },
    type: 'Income',
    payment_type: 'Debit',    
    payment_amount: 10000,
    payment_date: new Date('2016-05-14'),
    order_date: new Date('2016-05-14'),
    payment_date_limit: new Date('2016-05-15')
  },
  {
    title: 'Website',
    category: { name: 'Freelance', type: 'Income' },
    type: 'Income',
    payment_type: 'Debit',    
    payment_amount: 100,
    payment_date: new Date('2016-06-11'),
    order_date: new Date('2016-06-11'),
    payment_date_limit: null
  },
  {
    title: 'Apartment',
    category: { name: 'Rent', type: 'Income' },
    type: 'Income',
    payment_type: 'Debit',    
    payment_amount: 200,
    payment_date: new Date('2016-06-23'),
    order_date: new Date('2016-06-23'),
    payment_date_limit: new Date('2016-06-25')
  }
];

var categories = [
  { name: 'Public Services', type: 'Expense' },
  { name: 'Food', type: 'Expense' },
  { name: 'Entertainment', type: 'Expense' },
  { name: 'Home', type: 'Expense' },
  { name: 'Transport', type: 'Expense' },
  { name: 'Salary', type: 'Income' },
  { name: 'Website', type: 'Income' },
  { name: 'Apartment', type: 'Income' }
];

var users = [{
  profile: {
    email: 'fcog@hotmail.com',
    name: 'francisco',
    picture: 'http://graph.facebook.com/1744551055802043/picture?type=large'
  },
  data: {
    oauth: '1791573251120838'
  }
}];

describe('Statistics API', function() {
  var server;
  var Transaction;
  var Category;
  var User;

  // Load model and server
  before(function() {
    var app = express();

    // Bootstrap server
    var models = require('./../models/models')(wagner);

    // Make Category model available in tests
    Transaction = models.Transaction;
    Category = models.Category;
    User = models.User;

    app.use(function(req, res, next) {
      User.findOne({}, function(error, user) {
        assert.ifError(error);
        req.user = user;
        next();
      });
    });

    app.use(require('./../api/statisticsApi')(wagner));

    server = app.listen(3002);
  });

  after(function() {
    // Shut the server down when we're done
    server.close();
  });

  // Erase data
  beforeEach(function(done) {
    // Make sure docs are empty before each test
    Transaction.remove({}, function(error) {
      assert.ifError(error);
      Category.remove({}, function(error) {
        assert.ifError(error);
        User.remove({}, function(error) {
          assert.ifError(error);
          done();
        });
      });
    });
  });

  // Load data
  beforeEach(function(done) {

    // Load user
    User.create(users, function(error) {  
      User.findOne({}, function(error, user) {
        assert.ifError(error);
        // Load categories
        categories.forEach(function(element) {
          element.user = user.id;
        }, this);
        Category.create(categories, function(error) {
          assert.ifError(error);
          // Load transactions
          transactions.forEach(function(element) {
            element.user = user.id;
            element.category.user = user.id;
          }, this);
          Transaction.create(transactions, function(error) {
              assert.ifError(error);
              done();
          });
        });
      });  
    });   
  });

  it('can sum incomes grouped by month and year', function(done) {
    var type = 'income';
    var orderdir = 'asc';
    var url = URL_ROOT + '/statistics/sum?type=' + type + '&orderdir=' + orderdir;
    // Make an HTTP request to localhost:3000/statistics/sum/{month}
    superagent.get(url, function(error, res) {
        assert.ifError(error);
        var result;
        // And make sure we got the correct info back
        assert.doesNotThrow(function() {
            result = JSON.parse(res.text);
        });
        assert.ok(result);
        assert.equal(result[0].debit, 10000);
        assert.equal(result[0].credit, 0);
        assert.equal(result[0]._id.year, 2016);
        assert.equal(result[0]._id.month, 5);            
        assert.equal(result[1].debit, 300);
        assert.equal(result[1].credit, 0);
        assert.equal(result[1]._id.year, 2016);
        assert.equal(result[1]._id.month, 6);            
        done();
    });
  });

  it('can sum expenses gruped by month and year', function(done) {
    var type = 'expense';
    var orderdir = 'asc';
    var url = URL_ROOT + '/statistics/sum?type=' + type + '&orderdir=' + orderdir;
    // Make an HTTP request to localhost:3000/statistics/sum/{month}
    superagent.get(url, function(error, res) {
        assert.ifError(error);
        var result;
        // And make sure we got the correct info back
        assert.doesNotThrow(function() {
            result = JSON.parse(res.text);
        });
        assert.ok(result);
        assert.equal(result[0].debit, 100);
        assert.equal(result[0].credit, 0);
        assert.equal(result[0]._id.year, 2016);
        assert.equal(result[0]._id.month, 5);
        assert.equal(result[1].debit, 2000);
        assert.equal(result[1].credit, 1000);
        assert.equal(result[1]._id.year, 2016);
        assert.equal(result[1]._id.month, 6);        
        done();
    });
  });

  it('can sum incomes by two given dates', function(done) {
    var type = 'income';
    var dateStart = new Date('2016-06-11');
    var dateEnd = new Date('2016-06-27');
    var url = URL_ROOT + '/statistics/sum?type=' + type + '&date_start=' + dateStart + '&date_end=' + dateEnd;
    // Make an HTTP request to localhost:3000/statistics/sum/{month}
    superagent.get(url, function(error, res) {
        assert.ifError(error);
        var result;
        // And make sure we got the correct info back
        assert.doesNotThrow(function() {
            result = JSON.parse(res.text);
        });
        assert.ok(result);
        assert.equal(result[0].total, 300);         
        done();
    });
  });

  it('can sum expenses by two given dates', function(done) {
    var type = 'expense';
    var dateStart = new Date('2016-06-11');
    var dateEnd = new Date('2016-06-27');
    var url = URL_ROOT + '/statistics/sum?type=' + type + '&date_start=' + dateStart + '&date_end=' + dateEnd;
    // Make an HTTP request to localhost:3000/statistics/sum/{month}
    superagent.get(url, function(error, res) {
        assert.ifError(error);
        var result;
        // And make sure we got the correct info back
        assert.doesNotThrow(function() {
            result = JSON.parse(res.text);
        });
        assert.ok(result);
        assert.equal(result[0].total, 3000);       
        done();
    });
  });

  it('can sum expenses gruped by month, year and category', function(done) {
    var type = 'expense';
    var orderdir = 'asc';
    var url = URL_ROOT + '/statistics/category';
    // Make an HTTP request to localhost:3000/statistics/sum/{month}
    superagent.get(url, function(error, res) {
        assert.ifError(error);
        var result;
        // And make sure we got the correct info back
        assert.doesNotThrow(function() {
            result = JSON.parse(res.text);
        });
        assert.ok(result);
        assert.equal(result[0].sum, 100);
        assert.equal(result[0]._id.year, 2016);
        assert.equal(result[0]._id.month, 5);
        assert.equal(result[0]._id.category, 'Food');
        assert.equal(result[1].sum, 2000);
        assert.equal(result[1]._id.year, 2016);
        assert.equal(result[1]._id.month, 6);        
        assert.equal(result[1]._id.category, 'Personal'); 
        assert.equal(result[2].sum, 1000);
        assert.equal(result[2]._id.year, 2016);
        assert.equal(result[2]._id.month, 6);        
        assert.equal(result[2]._id.category, 'Food');                       
        done();
    });
  });

});