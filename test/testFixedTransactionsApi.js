var assert = require('assert');
var express = require('express');
var status = require('http-status');  
var superagent = require('superagent');
var wagner = require('wagner-core');

var URL_ROOT = 'http://localhost:3002';

var fixedTransactions = [
  {
    title: 'Salary',
    category: { name: 'Salary' },
    type: 'Income',
    payment_amount: 28000,
    repetition: {
      day: 25,
      type: 'Monthly'
    }
  },
  {
    title: 'Gas',
    category: { name: 'Public Services' },
    type: 'Expense',
    payment_amount: null,
    repetition: {
      day: 1,
      type: 'Monthly'
    }
  },
  {
    //urgent to be paid example
    title: 'Energy',
    category: { name: 'Public Services' },
    type: 'Expense',
    payment_amount: 2000,
    repetition: {
      day: 1,
      type: 'Monthly'
    }
  },
  {
    title: 'TV cable',
    category: { name: 'Other Services' },
    type: 'Expense',
    payment_amount: 10,
    repetition: {
      day: 10,
      type: 'Monthly'
    }
  }
];

var categories = [
  { name: 'Public Services' },
  { name: 'Food' },
  { name: 'Entertainment' },
  { name: 'Home' },
  { name: 'Transport' }
];

var users = [{
  profile: {
    email: 'fcog@hotmail.com',
    name: 'francisco',
    picture: 'http://graph.facebook.com/1744551055802043/picture?type=large'
  },
  data: {
    oauth: '1791573251120838'
  }
}];

describe('Fixed Transaction API', function() {
  var server;
  var FixedTransaction;
  var Category;
  var User;

  // Load model and server
  before(function() {
    var app = express();

    // Bootstrap server
    var models = require('./../models/models')(wagner);

    // Make Category model available in tests
    FixedTransaction = models.FixedTransaction;
    Category = models.Category;
    User = models.User;

    app.use(function(req, res, next) {
      User.findOne({}, function(error, user) {
        assert.ifError(error);
        req.user = user;
        next();
      });
    });

    app.use(require('./../api/fixedTransactionsApi')(wagner));

    server = app.listen(3002);
  });

  after(function() {
    // Shut the server down when we're done
    server.close();
  });

  // Erase data
  beforeEach(function(done) {
    // Make sure docs are empty before each test
    FixedTransaction.remove({}, function(error) {
      assert.ifError(error);
      Category.remove({}, function(error) {
        assert.ifError(error);
        User.remove({}, function(error) {
          assert.ifError(error);
          done();
        });
      });
    });
  });

  // Load data
  beforeEach(function(done) {

    // Load user
    User.create(users, function(error) {
      User.findOne({}, function(error, user) {
        assert.ifError(error);
        // Load categories
        categories[0].user = user.id;
        categories[1].user = user.id;
        categories[2].user = user.id;
        categories[3].user = user.id;
        categories[4].user = user.id;
        Category.create(categories, function(error) {
          assert.ifError(error);
          // Load transactions
          fixedTransactions[0].user = user.id;
          fixedTransactions[0].category.user = user.id;
          fixedTransactions[1].user = user.id;
          fixedTransactions[1].category.user = user.id;
          fixedTransactions[2].user = user.id;
          fixedTransactions[2].category.user = user.id;
          fixedTransactions[3].user = user.id;
          fixedTransactions[3].category.user = user.id;
          done();
        });
      });
    });
  });

  it('can load a fixed transaction by id', function(done) {
    // Create a transaction in DB
    FixedTransaction.create(fixedTransactions[0], function(error, fixedTransaction) {
      assert.ifError(error);
      var url = URL_ROOT + '/fixed-transactions/' + fixedTransaction._id;
      // Make an HTTP request to localhost:3000/transaction/id/{transaction_id}
      superagent.get(url, function(error, res) {
        assert.ifError(error);
        var result;
        // And make sure we got the correct info back
        assert.doesNotThrow(function() {
          result = JSON.parse(res.text);
        });
        assert.ok(result.fixedTransaction);
        assert.equal(result.fixedTransaction.title, fixedTransactions[0].title);
        assert.equal(result.fixedTransaction.type, fixedTransactions[0].type);
        assert.equal(result.fixedTransaction.payment_amount, fixedTransactions[0].payment_amount);
        assert.equal(result.fixedTransaction.repetition.day, fixedTransactions[0].repetition.day);
        assert.equal(result.fixedTransaction.repetition.type, fixedTransactions[0].repetition.type);
        assert.equal(result.fixedTransaction.category.name, fixedTransactions[0].category.name);
        assert.equal(result.fixedTransaction.user, fixedTransactions[0].user);
        done();
      });
    });
  });

  it('can load fixed transactions', function(done) {
    // Create 4 fixed transactions
    FixedTransaction.create(fixedTransactions, function(error, doc) {
      assert.ifError(error);
      var url = URL_ROOT + '/fixed-transactions';
      // Make an HTTP request to localhost:3000/transactions
      superagent.get(url, function(error, res) {
        assert.ifError(error);
        var result;
        // And make sure we got info back
        assert.doesNotThrow(function() {
          result = JSON.parse(res.text);
        });
        assert.ok(result.fixedTransactions);
        assert.equal(result.fixedTransactions.length, 4);
        assert.equal(result.fixedTransactions[0].title, fixedTransactions[0].title);
        assert.equal(result.fixedTransactions[0].type, fixedTransactions[0].type);
        assert.equal(result.fixedTransactions[0].payment_amount, fixedTransactions[0].payment_amount);
        assert.equal(result.fixedTransactions[0].repetition.day, fixedTransactions[0].repetition.day);
        assert.equal(result.fixedTransactions[0].repetition.type, fixedTransactions[0].repetition.type);
        assert.equal(result.fixedTransactions[0].category.name, fixedTransactions[0].category.name);
        assert.equal(result.fixedTransactions[0].user, fixedTransactions[0].user);
        assert.equal(result.fixedTransactions[1].title, fixedTransactions[1].title);
        assert.equal(result.fixedTransactions[1].type, fixedTransactions[1].type);
        assert.equal(result.fixedTransactions[1].payment_amount, fixedTransactions[1].payment_amount);
        assert.equal(result.fixedTransactions[1].repetition.day, fixedTransactions[1].repetition.day);
        assert.equal(result.fixedTransactions[1].repetition.type, fixedTransactions[1].repetition.type);
        assert.equal(result.fixedTransactions[1].category.name, fixedTransactions[1].category.name);
        assert.equal(result.fixedTransactions[1].user, fixedTransactions[1].user);
        assert.equal(result.fixedTransactions[2].title, fixedTransactions[2].title);
        assert.equal(result.fixedTransactions[2].type, fixedTransactions[2].type);
        assert.equal(result.fixedTransactions[2].payment_amount, fixedTransactions[2].payment_amount);
        assert.equal(result.fixedTransactions[2].repetition.day, fixedTransactions[2].repetition.day);
        assert.equal(result.fixedTransactions[2].repetition.type, fixedTransactions[2].repetition.type);
        assert.equal(result.fixedTransactions[2].category.name, fixedTransactions[2].category.name);
        assert.equal(result.fixedTransactions[2].user, fixedTransactions[2].user);
        done();
      });
    });
  });

  it('can load fixed transactions ordered by title asc', function(done) {
    // Create 4 transactions
    FixedTransaction.create(fixedTransactions, function(error, doc) {
      assert.ifError(error);
      var orderby = 'title';
      var orderdir = 'asc';
      var url = URL_ROOT + '/fixed-transactions?orderby=' + orderby + '&orderdir=' + orderdir;

      superagent.get(url, function(error, res) {
        assert.ifError(error);
        var result;
        // And make sure we got info back
        assert.doesNotThrow(function() {
          result = JSON.parse(res.text);
        });
        assert.ok(result.fixedTransactions);
        assert.equal(result.fixedTransactions[0].title, fixedTransactions[2].title);
        assert.equal(result.fixedTransactions[1].title, fixedTransactions[1].title);
        assert.equal(result.fixedTransactions[2].title, fixedTransactions[0].title);
        assert.equal(result.fixedTransactions[3].title, fixedTransactions[3].title);
        done();
      });
    });
  });

  it('can load fixed transactions ordered by title desc', function(done) {
    // Create 4 transactions
    FixedTransaction.create(fixedTransactions, function(error, doc) {
      assert.ifError(error);
      var orderby = 'title';
      var orderdir = 'desc';
      var url = URL_ROOT + '/fixed-transactions?orderby=' + orderby + '&orderdir=' + orderdir;

      superagent.get(url, function(error, res) {
        assert.ifError(error);
        var result;
        // And make sure we got info back
        assert.doesNotThrow(function() {
          result = JSON.parse(res.text);
        });
        assert.ok(result.fixedTransactions);
        assert.equal(result.fixedTransactions[0].title, fixedTransactions[3].title);
        assert.equal(result.fixedTransactions[1].title, fixedTransactions[0].title);
        assert.equal(result.fixedTransactions[2].title, fixedTransactions[1].title);
        assert.equal(result.fixedTransactions[3].title, fixedTransactions[2].title);
        done();
      });
    });
  });

  it('can save a fixed transaction with existing category', function(done) {
    var url = URL_ROOT + '/fixed-transactions';
    //save the fixed transaction via HTTP
    superagent.
    post(url).
    send({ fixedTransaction: fixedTransactions[0] }).
    end(function(error, res) {
      assert.ifError(error);
      assert.equal(res.status, status.OK);
      //Get the fixed transaction from the DB and check if it saved correctly
      FixedTransaction.findOne({}, function(error, fixedTransaction) {
        assert.ifError(error);
        assert.ok(fixedTransaction);
        assert.equal(fixedTransaction.title, fixedTransactions[0].title);
        assert.equal(fixedTransaction.category.name, fixedTransactions[0].category.name);
        assert.equal(fixedTransaction.category.user, fixedTransactions[0].user);
        assert.equal(fixedTransaction.type, fixedTransactions[0].type);
        assert.equal(fixedTransaction.payment_amount, fixedTransactions[0].payment_amount);
        assert.equal(fixedTransaction.repetition.day, fixedTransactions[0].repetition.day);
        assert.equal(fixedTransaction.repetition.type, fixedTransactions[0].repetition.type);
        assert.equal(fixedTransaction.user, fixedTransactions[0].user);

        //Get the just saved category from the DB and check if it saved correctly
        Category.find({ name: fixedTransactions[0].category.name, user: fixedTransactions[0].user }, function(error, categories) {
          assert.ifError(error);
          assert.ok(categories);
          assert.ok(categories.length, 1);
          done();
        });
      });
    });
  });

  it('can save a fixed transaction with new category', function(done) {
    var url = URL_ROOT + '/fixed-transactions';
    //save the fixed transaction via HTTP
    superagent.
    post(url).
    send({ fixedTransaction: fixedTransactions[3] }).
    end(function(error, res) {
      assert.ifError(error);
      assert.equal(res.status, status.OK);
      //Get the fixed transaction from the DB and check if it saved correctly
      FixedTransaction.findOne({}, function(error, fixedTransaction) {
        assert.ifError(error);
        assert.ok(fixedTransaction);
        assert.equal(fixedTransaction.title, fixedTransactions[3].title);
        assert.equal(fixedTransaction.category.name, fixedTransactions[3].category.name);
        assert.equal(fixedTransaction.category.user, fixedTransactions[3].user);
        assert.equal(fixedTransaction.type, fixedTransactions[3].type);
        assert.equal(fixedTransaction.payment_amount, fixedTransactions[3].payment_amount);
        assert.equal(fixedTransaction.repetition.day, fixedTransactions[3].repetition.day);
        assert.equal(fixedTransaction.repetition.type, fixedTransactions[3].repetition.type);
        assert.equal(fixedTransaction.user, fixedTransactions[3].user);

        //Get the just saved category from the DB and check if it saved correctly
        Category.find({ name: fixedTransactions[3].category.name, user: fixedTransactions[3].user }, function(error, categories) {
          assert.ifError(error);
          assert.ok(categories);
          assert.ok(categories.length, 1);
          done();
        });
      });
    });
  });

  it('can delete a fixed transaction', function(done) {
    //save a transaction to the DB
    FixedTransaction.create(fixedTransactions[0], function(error, fixedTransactionSaved) {
      assert.ifError(error);
      //check if saved
      assert.equal(fixedTransactionSaved.title, fixedTransactions[0].title);
      //delete the transaction via HTTP
      var url = URL_ROOT + '/fixed-transactions/' + fixedTransactionSaved.id;
      superagent.
      del(url).
      send({}).
      end(function(error, res) {
        assert.ifError(error);
        assert.equal(res.status, status.OK);

        //Get the transaction from the DB and check if it returns null
        FixedTransaction.findOne({ _id: fixedTransactionSaved.id }, function(error, fixedTransaction) {
          assert.ifError(error);
          assert.equal(fixedTransaction, null);
          done();
        });
      });
    });
  });

  it('can update a fixed transaction with existing category', function(done) {

    var fixedTransactionUpdate =  {
      _id: fixedTransactions[0].id,
      title: 'Rent',
      category: { name: 'Home' },
      type: 'Income',
      payment_amount: 700,
      repetition: { day: 10, type: 'weekly' }
    };

    //save a fixed transaction to the DB
    FixedTransaction.create(fixedTransactions[0], function(error, fixedTransactionSaved) {
      assert.ifError(error);
      //check if saved
      assert.equal(fixedTransactionSaved.title, fixedTransactions[0].title);
      //update the fixed transaction via HTTP
      var url = URL_ROOT + '/fixed-transactions/' + fixedTransactionSaved.id;
      superagent.
      put(url).
      send({ fixedTransaction: fixedTransactionUpdate }).
      end(function(error, res) {
        assert.ifError(error);
        assert.equal(res.status, status.OK);

        //Get the fixed transaction from the DB and check if it updated
        FixedTransaction.findOne({ _id: fixedTransactionSaved.id }, function(error, fixedTransactionUpdated) {
          assert.ifError(error);
          assert.ok(fixedTransactionUpdated);
          assert.equal(fixedTransactionUpdated.title, fixedTransactionUpdate.title);
          assert.equal(fixedTransactionUpdated.category.name, fixedTransactionUpdate.category.name);
          assert.equal(fixedTransactionUpdated.payment_amount, fixedTransactionUpdate.payment_amount);
          assert.equal(fixedTransactionUpdated.repetition.day, fixedTransactionUpdate.repetition.day);
          assert.equal(fixedTransactionUpdated.repetition.type, fixedTransactionUpdate.repetition.type);
          Category.find({ name: fixedTransactionUpdate.category.name, user: fixedTransactions[0].user }, function(error, categories) {
            assert.ifError(error);
            assert.ok(categories);
            assert.ok(categories.length, 1);
            done();
          });
        });
      });
    });
  });

  it('can update a fixed transaction with new category', function(done) {

    var fixedTransactionUpdate =  {
      _id: fixedTransactions[0].id,
      title: 'Rent',
      category: { name: 'New Category' },
      type: 'Income',
      payment_amount: 700,
      repetition: { day: 10, type: 'weekly' }
    };

    //save a fixed transaction to the DB
    FixedTransaction.create(fixedTransactions[0], function(error, fixedTransactionSaved) {
      assert.ifError(error);
      //check if saved
      assert.equal(fixedTransactionSaved.title, fixedTransactions[0].title);
      //update the fixed transaction via HTTP
      var url = URL_ROOT + '/fixed-transactions/' + fixedTransactionSaved.id;
      superagent.
      put(url).
      send({ fixedTransaction: fixedTransactionUpdate }).
      end(function(error, res) {
        assert.ifError(error);
        assert.equal(res.status, status.OK);

        //Get the fixed transaction from the DB and check if it updated
        FixedTransaction.findOne({ _id: fixedTransactionSaved.id }, function(error, fixedTransactionUpdated) {
          assert.ifError(error);
          assert.ok(fixedTransactionUpdated);
          assert.equal(fixedTransactionUpdated.title, fixedTransactionUpdate.title);
          assert.equal(fixedTransactionUpdated.category.name, fixedTransactionUpdate.category.name);
          assert.equal(fixedTransactionUpdated.payment_amount, fixedTransactionUpdate.payment_amount);
          assert.equal(fixedTransactionUpdated.repetition.day, fixedTransactionUpdate.repetition.day);
          assert.equal(fixedTransactionUpdated.repetition.type, fixedTransactionUpdate.repetition.type);
          Category.find({ name: fixedTransactionUpdate.category.name, user: fixedTransactions[0].user }, function(error, categories) {
            assert.ifError(error);
            assert.ok(categories);
            assert.ok(categories.length, 1);
            done();
          });
        });
      });
    });
  });

  it('Seed database', function(done) {
    FixedTransaction.create(fixedTransactions[0]);
    FixedTransaction.create(fixedTransactions[1]);
    FixedTransaction.create(fixedTransactions[2]);
    FixedTransaction.create(fixedTransactions[3]);
    assert.equal(true, true);
    done();
  });
});