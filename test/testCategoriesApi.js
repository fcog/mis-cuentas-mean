var assert = require('assert');
var express = require('express');
var status = require('http-status');  
var superagent = require('superagent');
var wagner = require('wagner-core');

var URL_ROOT = 'http://localhost:3002';

var categories = [
  { name: 'Public Services', type: 'Expense' },
  { name: 'Food', type: 'Expense' },
  { name: 'Entertainment', type: 'Expense' },
  { name: 'Home', type: 'Expense' },
  { name: 'Transport', type: 'Expense' },
  { name: 'Salary', type: 'Income' },
  { name: 'Taxes', type: 'Expense' },
  { name: 'Rent', type: 'Income' }
];

var users = [{
  profile: {
    email: 'fcog@hotmail.com',
    name: 'francisco',
    picture: 'http://graph.facebook.com/1744551055802043/picture?type=large'
  },
  data: {
    oauth: '1791573251120838'
  }
}];

describe('Category API', function() {
  var server;
  var Category;
  var User;

  // Load model and server
  before(function() {
    var app = express();

    // Bootstrap server
    var models = require('./../models/models')(wagner);

    // Make Category model available in tests
    Category = models.Category;
    User = models.User;

    app.use(function(req, res, next) {
      User.findOne({}, function(error, user) {
        assert.ifError(error);
        req.user = user;
        next();
      });
    });

    app.use(require('./../api/CategoriesApi')(wagner));

    server = app.listen(3002);
  });

  after(function() {
    // Shut the server down when we're done
    server.close();
  });

  // Erase data
  beforeEach(function(done) {
    // Make sure docs are empty before each test
    Category.remove({}, function(error) {
      assert.ifError(error);
      User.remove({}, function(error) {
        assert.ifError(error);
        done();
      });
    });
  });

  // Load data
  beforeEach(function(done) {

    // Load user
    User.create(users, function(error) {
      User.findOne({}, function(error, user) {
        assert.ifError(error);
        // associate the categories to the user
        categories[0].user = user.id;
        categories[1].user = user.id;
        categories[2].user = user.id;
        categories[3].user = user.id;
        categories[4].user = user.id;
        categories[5].user = user.id;
        categories[6].user = user.id;
        categories[7].user = user.id;
        done();
      });
    });
  });

  it('can load a category by type', function(done) {
    // Create a transaction in DB
    Category.create(categories, function(error, category) {
      assert.ifError(error);
      //Check expense
      var url = URL_ROOT + '/categories?type=Expense';
      // Make an HTTP request to localhost:3000/categories?type=Expense
      superagent.get(url, function(error, res) {
        assert.ifError(error);
        var result;
        // And make sure we got the correct info back
        assert.doesNotThrow(function() {
          result = JSON.parse(res.text);
        });
        assert.ok(result.categories);
        assert.equal(result.categories.length, 6);

        //Check income
        url = URL_ROOT + '/categories?type=Income';
        // Make an HTTP request to localhost:3000/categories?type=Income
        superagent.get(url, function(error, res) {
          assert.ifError(error);
          var result;
          // And make sure we got the correct info back
          assert.doesNotThrow(function() {
            result = JSON.parse(res.text);
          });
          assert.ok(result.categories);
          assert.equal(result.categories.length, 2);
          done();
        });
      });
    });
  });

});