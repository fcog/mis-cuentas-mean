var assert = require('assert');
var express = require('express');
var status = require('http-status');  
var superagent = require('superagent');
var wagner = require('wagner-core');

var URL_ROOT = 'http://localhost:3002';

var transactions = [
  {
    title: 'Dinner',
    category: { name: 'Food', type: 'Expense' },
    type: 'Expense',
    payment_type: 'Debit',
    payment_amount: 1000,
    payment_date: new Date('2016-05-20'),
    order_date: new Date('2016-05-20'),
    payment_date_limit: new Date('2016-05-22')
  },    
  {
    title: 'Shopping',
    category: { name: 'Personal', type: 'Expense' },
    type: 'Expense',
    payment_type: 'Debit',
    payment_amount: 2000,
    payment_date: new Date('2016-06-15'),
    order_date: new Date('2016-06-15'),
    payment_date_limit: new Date('2016-06-22')
  },
  {
    //urgent to be paid example
    title: 'Energy',
    category: { name: 'Public Services', type: 'Expense' },
    type: 'Expense',
    payment_amount: null,
    order_date: new Date('2016-05-27'),
    payment_date_limit: new Date('2016-05-27'),
    urgent: true
  },
  {
    title: 'Ice Cream',
    category: { name: 'Food', type: 'Expense' },
    type: 'Expense',
    payment_type: 'Debit',    
    payment_amount: 10,
    payment_date: new Date('2016-06-11'),
    order_date: new Date('2016-06-11'),
    payment_date_limit: new Date('2016-06-15')
  }
];

var categories = [
  { name: 'Public Services', type: 'Expense' },
  { name: 'Food', type: 'Expense' },
  { name: 'Entertainment', type: 'Expense' },
  { name: 'Home', type: 'Expense' },
  { name: 'Transport', type: 'Expense' }
];

var users = [{
  profile: {
    email: 'fcog@hotmail.com',
    name: 'francisco',
    picture: 'http://graph.facebook.com/1744551055802043/picture?type=large'
  },
  data: {
    oauth: '1791573251120838'
  }
}];

describe('Transaction API', function() {
  var server;
  var Transaction;
  var Category;
  var User;

  // Load model and server
  before(function() {
    var app = express();

    // Bootstrap server
    var models = require('./../models/models')(wagner);

    // Make Category model available in tests
    Transaction = models.Transaction;
    Category = models.Category;
    User = models.User;

    app.use(function(req, res, next) {
      User.findOne({}, function(error, user) {
        assert.ifError(error);
        req.user = user;
        next();
      });
    });

    app.use(require('./../api/transactionsApi')(wagner));

    server = app.listen(3002);
  });

  after(function() {
    // Shut the server down when we're done
    server.close();
  });

  // Erase data
  beforeEach(function(done) {
    // Make sure docs are empty before each test
    Transaction.remove({}, function(error) {
      assert.ifError(error);
      Category.remove({}, function(error) {
        assert.ifError(error);
        User.remove({}, function(error) {
          assert.ifError(error);
          done();
        });
      });
    });
  });

  // Load data
  beforeEach(function(done) {

    // Load user
    User.create(users, function(error) {  
      User.findOne({}, function(error, user) {
        assert.ifError(error);
        // Load categories
        categories[0].user = user.id;
        categories[1].user = user.id;
        categories[2].user = user.id;
        categories[3].user = user.id;
        categories[4].user = user.id;
        Category.create(categories, function(error) {
          assert.ifError(error);
          // Load transactions
          transactions[0].user = user.id;
          transactions[0].category.user = user.id;
          transactions[1].user = user.id;
          transactions[1].category.user = user.id;
          transactions[2].user = user.id;
          transactions[2].category.user = user.id;
          transactions[3].user = user.id;
          transactions[3].category.user = user.id;
          done();
        });
      });  
    });   
  });

  it('can load a transaction by id', function(done) {
    // Create a transaction in DB
    Transaction.create(transactions[0], function(error, transaction) {
      assert.ifError(error);
      var url = URL_ROOT + '/transactions/' + transaction._id;
      // Make an HTTP request to localhost:3000/transaction/id/{transaction_id}
      superagent.get(url, function(error, res) {
        assert.ifError(error);
        var result;
        // And make sure we got the correct info back
        assert.doesNotThrow(function() {
          result = JSON.parse(res.text);
        });
        assert.ok(result.transaction);
        assert.equal(result.transaction.title, transactions[0].title);
        assert.equal(result.transaction.type, transactions[0].type);
        assert.equal(result.transaction.payment_amount, transactions[0].payment_amount);
        assert.equal(result.transaction.payment_type, transactions[0].payment_type);
        assert.equal(result.transaction.payment_date, '2016-05-20T00:00:00.000Z');
        assert.equal(result.transaction.payment_date_limit, '2016-05-22T00:00:00.000Z');
        assert.equal(result.transaction.category.name, transactions[0].category.name);
        assert.equal(result.transaction.user, transaction.user);
        done();
      });
    });
  });

  it('can load transactions', function(done) {
    // Create 4 transactions
    Transaction.create(transactions, function(error, doc) {
      assert.ifError(error);
      var url = URL_ROOT + '/transactions';
      // Make an HTTP request to localhost:3000/transactions
      superagent.get(url, function(error, res) {
        assert.ifError(error);
        var result;
        // And make sure we got info back
        assert.doesNotThrow(function() {
          result = JSON.parse(res.text);
        });
        assert.ok(result.transactions);
        assert.equal(result.transactions.length, 4);
        assert.equal(result.transactions[0].title, 'Energy');
        assert.equal(result.transactions[0].payment_amount, null);
        assert.equal(result.transactions[0].payment_date_limit, '2016-05-27T00:00:00.000Z');
        assert.equal(result.transactions[0].category.name, 'Public Services');      
        assert.equal(result.transactions[1].title, 'Dinner');
        assert.equal(result.transactions[1].type, 'Expense');
        assert.equal(result.transactions[1].payment_amount, 1000);
        assert.equal(result.transactions[1].payment_type, 'Debit');
        assert.equal(result.transactions[1].payment_date, '2016-05-20T00:00:00.000Z');
        assert.equal(result.transactions[1].payment_date_limit, '2016-05-22T00:00:00.000Z');
        assert.equal(result.transactions[1].category.name, 'Food');
        assert.equal(result.transactions[1].user, transactions[0].user);              
        assert.equal(result.transactions[3].title, 'Ice Cream');
        assert.equal(result.transactions[3].payment_amount, 10);
        assert.equal(result.transactions[3].payment_date, '2016-06-11T00:00:00.000Z');
        assert.equal(result.transactions[3].payment_date_limit, '2016-06-15T00:00:00.000Z');    
        assert.equal(result.transactions[3].category.name, 'Food');                    
        done();
      });
    });
  });

  it('can load transactions after a given payment date', function(done) {
    // Create 4 transactions
    Transaction.create(transactions, function(error, doc) {
      assert.ifError(error);
      var date_start = new Date('2016-06-11');
      var url = URL_ROOT + '/transactions?date_start=' + date_start;
      // Make an HTTP request to localhost:3000/transactions?date_start=2016-06-20
      superagent.get(url, function(error, res) {
        assert.ifError(error);
        var result;
        // And make sure we got info back
        assert.doesNotThrow(function() {
          result = JSON.parse(res.text);
        });
        assert.ok(result.transactions);
        assert.equal(result.transactions.length, 3);
        done();
      });
    });
  });

  it('can load transactions between two given payment dates', function(done) {
    // Create 4 transactions
    Transaction.create(transactions, function(error, doc) {
      assert.ifError(error);
      var date_start = new Date('2016-06-11');
      var date_end = new Date('2016-06-27');
      var url = URL_ROOT + '/transactions?date_start=' + date_start + '&date_end=' + date_end;
      // Make an HTTP request to localhost:3000/transactions?date_start=2016-06-20&date_end=2016-06-30
      superagent.get(url, function(error, res) {
        assert.ifError(error);
        var result;
        // And make sure we got info back
        assert.doesNotThrow(function() {
          result = JSON.parse(res.text);
        });
        assert.ok(result.transactions);
        assert.equal(result.transactions.length, 3);
        done();
      });
    });
  });

  it('can load transactions ordered by order date asc', function(done) {
    // Create 4 transactions
    Transaction.create(transactions, function(error, doc) {
      assert.ifError(error);
      var orderby = 'order_date';
      var orderdir = 'asc';
      var url = URL_ROOT + '/transactions?orderby=' + orderby + '&orderdir=' + orderdir;
      superagent.get(url, function(error, res) {
        assert.ifError(error);
        var result;
        // And make sure we got info back
        assert.doesNotThrow(function() {
          result = JSON.parse(res.text);
        });
        assert.ok(result.transactions);
        assert.equal(result.transactions[0].title, 'Energy');        
        assert.equal(result.transactions[1].title, 'Dinner');
        assert.equal(result.transactions[2].title, 'Ice Cream');
        assert.equal(result.transactions[3].title, 'Shopping');
        done();
      });
    });
  });

  it('can load transactions ordered by order date desc', function(done) {
    // Create 4 transactions
    Transaction.create(transactions, function(error, doc) {
      assert.ifError(error);
      var orderby = 'order_date';
      var orderdir = 'desc';
      var url = URL_ROOT + '/transactions?orderby=' + orderby + '&orderdir=' + orderdir;
      superagent.get(url, function(error, res) {
        assert.ifError(error);
        var result;
        // And make sure we got info back
        assert.doesNotThrow(function() {
          result = JSON.parse(res.text);
        });
        assert.ok(result.transactions);
        assert.equal(result.transactions[0].title, 'Energy');        
        assert.equal(result.transactions[1].title, 'Shopping');
        assert.equal(result.transactions[2].title, 'Ice Cream');
        assert.equal(result.transactions[3].title, 'Dinner');
        done();
      });
    });
  });

  //check how many transactions are urgent (the payment date limit of a transaction has less than 4 days to be paid)
  it('can load transactions that are urgent to be paid', function(done) {
    // Create 4 transactions
    Transaction.create(transactions, function(error, doc) {
      assert.ifError(error);
      var url = URL_ROOT + '/transactions?urgent=true';
      // Make an HTTP request to localhost:3000/transactions?urgent=true
      superagent.get(url, function(error, res) {
        assert.ifError(error);
        var result;
        // And make sure we got info back
        assert.doesNotThrow(function() {
          result = JSON.parse(res.text);
        });
        assert.ok(result.transactions);
        assert.equal(result.transactions.length, 1);
        done();
      });
    });
  });

  it('can save a transaction with existing category', function(done) {
    var url = URL_ROOT + '/transactions';
    //save the transaction via HTTP
    superagent.
      post(url).
      send({ transaction: transactions[0] }).
      end(function(error, res) {
        assert.ifError(error);
        assert.equal(res.status, status.OK);
        //Get the category from the DB and check if it exists
        Category.find({ name: transactions[0].category.name, user: transactions[0].user }, function(error, categories) {
          assert.ifError(error);
          assert.ok(categories);
          assert.ok(categories.length, 1);
          Transaction.findOne({}, function(error, transaction) {
            assert.ifError(error);
            assert.ok(transaction);
            assert.equal(transaction.title, transactions[0].title);
            assert.equal(transaction.category.name, transactions[0].category.name);
            assert.equal(transaction.category.user, transactions[0].user);
            assert.equal(transaction.type, transactions[0].type);
            assert.equal(transaction.payment_amount, transactions[0].payment_amount);
            assert.equal(transaction.payment_type, transactions[0].payment_type);
            assert.equal(transaction.user, transactions[0].user);
            assert.deepEqual(transaction.payment_date, transactions[0].payment_date);
            assert.deepEqual(transaction.payment_date_limit, transactions[0].payment_date_limit);
            //Get the just saved category from the DB and check if it didn't save a new one
            Category.find({ name: transactions[0].category.name, user: transactions[0].user }, function(error, categories) {
              assert.ifError(error);
              assert.ok(categories);
              assert.ok(categories.length, 1);
              done();
            });
          });
        });
      }); 
  });

  it('can save a transaction with new category', function(done) {
    var url = URL_ROOT + '/transactions';
    //save the transaction via HTTP
    superagent.
      post(url).
      send({ transaction: transactions[1] }).
      end(function(error, res) {
        assert.ifError(error);
        assert.equal(res.status, status.OK);
        //Get the category from the DB and check if it doesn't exists
        Category.find({ name: transactions[0].category.name, user: transactions[0].user }, function(error, categories) {
          assert.ifError(error);
          assert.ok(categories);
          assert.ok(categories.length, 0);
          //Get the transaction from the DB and check if it saved correctly
          Transaction.findOne({}, function(error, transaction) {
            assert.ifError(error);
            assert.ok(transaction);
            assert.equal(transaction.title, transactions[1].title);
            assert.equal(transaction.category.name, transactions[1].category.name);
            assert.equal(transaction.category.user, transactions[1].user);
            assert.equal(transaction.type, transactions[1].type);
            assert.equal(transaction.payment_amount, transactions[1].payment_amount);
            assert.equal(transaction.payment_type, transactions[1].payment_type);
            assert.equal(transaction.user, transactions[1].user);
            assert.deepEqual(transaction.payment_date, transactions[1].payment_date);
            assert.deepEqual(transaction.payment_date_limit, transactions[1].payment_date_limit);
            //Get the just saved category from the DB and check if it saved correctly
            Category.find({name: transactions[1].category.name, user: transactions[1].user}, function(error, categories) {
              assert.ifError(error);
              assert.ok(categories);
              assert.ok(categories.length, 1);
              assert.ok(categories[0].type, transaction.type);
              done();
            });
          });
        });
      }); 
  });  

  it('can delete a transaction', function(done) {
    //save a transaction to the DB
    Transaction.create(transactions[0], function(error, transactionSaved) {
      assert.ifError(error);
      //check if saved
      assert.equal(transactionSaved.title, transactions[0].title);
      //delete the transaction via HTTP
      var url = URL_ROOT + '/transactions/' + transactionSaved.id;
      superagent.
        del(url).
        send({}).
        end(function(error, res) {
          assert.ifError(error);
          assert.equal(res.status, status.OK);

          //Get the transaction from the DB and check if it returns null
          Transaction.findOne({ _id: transactions[0].id }, function(error, transaction) {
            assert.ifError(error);
            assert.equal(transaction, null);
            done();
          });
        }); 
      });      
  }); 

  it('can update a transaction with existing category', function(done) {
    //save a transaction to the DB
    Transaction.create(transactions[0], function(error, transactionSaved) {
      assert.ifError(error);
      //check if saved
      assert.equal(transactionSaved.title, transactions[0].title);
      //update the transaction via HTTP
      var url = URL_ROOT + '/transactions/' + transactionSaved.id;
      superagent.
        put(url).
        send({
          transaction: { 
              _id: transactions[0].id, 
              title: 'Gas', 
              category:  { name: 'Transport', type: 'Expense' },
              payment_type: 'Credit',  
              payment_amount: 200,
              payment_date: new Date('2016-07-02'),
              payment_date_limit: new Date('2016-07-05'),
            }
        }).
        end(function(error, res) {
          assert.ifError(error);
          assert.equal(res.status, status.OK);

          //Get the transaction from the DB and check if it updated
          Transaction.findOne({ _id: transactions[0].id }, function(error, transaction) {
            assert.ifError(error);
            assert.ok(transaction);
            assert.equal(transaction.title, 'Gas');
            assert.equal(transaction.category.name, 'Transport');
            assert.equal(transaction.payment_type, 'Credit');
            assert.equal(transaction.payment_amount, 200);
            assert.deepEqual(transaction.payment_date, new Date('2016-07-02'));
            assert.deepEqual(transaction.payment_date_limit, new Date('2016-07-05'));
            Category.find({ name: 'Transport', user: transactions[0].user }, function(error, categories) {
              assert.ifError(error);
              assert.ok(categories);
              assert.ok(categories.length, 1);
              assert.ok(categories[0].type, transaction.type);
              done();
            });
          });
        });
    });
  });  

  it('can update a transaction with new category', function(done) {
    //save a transaction to the DB
    Transaction.create(transactions[0], function(error, transactionSaved) {
      assert.ifError(error);
      //check if saved
      assert.equal(transactionSaved.title, transactions[0].title);
      //update the transaction via HTTP
      var url = URL_ROOT + '/transactions/' + transactionSaved.id;
      superagent.
        put(url).
        send({
          transaction: { 
              _id: transactions[0].id, 
              title: 'Ball', 
              category:  { name: 'Gifts' },
              type: 'Expense',
              payment_type: 'Credit',  
              payment_amount: 20,
              payment_date: new Date('2016-07-02'),
              payment_date_limit: new Date('2016-07-05'),
            }
        }).
        end(function(error, res) {
          assert.ifError(error);
          assert.equal(res.status, status.OK);

          //Get the transaction from the DB and check if it updated
          Transaction.findOne({ _id: transactions[0].id }, function(error, transaction) {
            assert.ifError(error);
            assert.ok(transaction);
            assert.equal(transaction.title, 'Ball');
            assert.equal(transaction.category.name, 'Gifts');
            assert.equal(transaction.payment_type, 'Credit');
            assert.equal(transaction.payment_amount, 20);
            assert.deepEqual(transaction.payment_date, new Date('2016-07-02'));
            assert.deepEqual(transaction.payment_date_limit, new Date('2016-07-05'));
            Category.find({ name: 'Gifts', type: transaction.type, user: transactions[0].user }, function(error, categories) {
              assert.ifError(error);
              assert.ok(categories);
              assert.ok(categories.length, 1);
              assert.ok(categories[0].type, transaction.type);
              done();
            });
          });
        });
    });
  });

  it('can return the first and last transactions paid', function(done) {
    // Create 4 transactions
    Transaction.create(transactions, function(error, doc) {
      assert.ifError(error);
      var url = URL_ROOT + '/transactions/first-last';
      // Make an HTTP request to localhost:3000/transactions
      superagent.get(url, function(error, res) {
        assert.ifError(error);
        var result;
        // And make sure we got info back
        assert.doesNotThrow(function() {
          result = JSON.parse(res.text);
        });
        assert.ok(result.transactions);
        assert.equal(result.transactions.length, 2);
        assert.equal(result.transactions[0].id, transactions[0].id);  
        assert.equal(result.transactions[1].id, transactions[1].id);                              
        done();
      });
    });
  });

  it('Seed database', function(done) {
    Transaction.create(transactions[0]);
    Transaction.create(transactions[1]);
    Transaction.create(transactions[2]);
    Transaction.create(transactions[3]);
    assert.equal(true, true);
    done();
  });
});