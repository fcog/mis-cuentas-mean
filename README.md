#Compile browser JS
##run auto:
1. cd public
2. npm run watch
##or run manually
1. cd public
2. ./node_modules/.bin/browserify -o bin/index.js index.js

#Test browser code
./node_modules/karma/bin/karma start ./karma.local.conf.js
##OR
npm test

#Run server
node server.js

#Test server code
##run auto:
npm run watch
##or run manually
npm run test-transactions
npm run test-fixedtransactions
npm run test-categories
npm run test-statistics