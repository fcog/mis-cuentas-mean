var fs = require( 'fs' );

function getConfig() {
	return JSON.parse( fs.readFileSync( `${process.cwd()}/config.json` ).toString() );
};

module.exports = getConfig;