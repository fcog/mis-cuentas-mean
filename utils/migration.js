var sqlite3 = require( 'sqlite3' ).verbose();
var mongoose = require( 'mongoose' );

module.exports = function( User, FixedTransaction, Transaction, Category ) {
		var db = new sqlite3.Database('database.sqlite');

		Category.remove({});
		Transaction.remove({});

		db.serialize(function() {

			db.each("SELECT * FROM categories", function(err, row) {
				var userId = mongoose.Types.ObjectId('57c5d6593f52bf33cf094772');
				var type = (row.type_id == 1) ? 'Expense' : 'Income';
				var category = {
					'updatedAt': row.updated_at,
					'createdAt': row.created_at,
					'name': row.title,
					'type': type,
					'user': userId
				}

				Category.create(category, function(err, categorySaved) {
			    	if (err) {
			    		console.log("Error syncing category id: " + row.id + " Error" + err);
			    	}
				  	if (categorySaved) {
				  		//console.log('Category synced.')
				  	}
				});
			});

			db.each("SELECT t.*, c.title as category_title FROM transactions t LEFT JOIN categories c ON c.id = t.category_id", function(err, row) {
				var userId = mongoose.Types.ObjectId('57c5d6593f52bf33cf094772');
				var type = (row.type_id == 1) ? 'Expense' : 'Income';
				var payment_type = (row.payment_type_id == null) ? null : (row.payment_type_id == 1) ? 'Debit' : 'Credit';
				var category = {
					'name': row.category_title,
					'type': type,
					'user': userId
				}
				var payment_date = (row.paid_date) ? new Date(row.paid_date) : null;
				var payment_date_limit = (row.payment_date_limit) ? new Date(row.payment_date_limit) : null;
				var transaction = {
					'updatedAt': row.updated_at,
					'createdAt': row.created_at,
					'title': row.title,
					'type': type,
					'payment_amount': row.amount_paid,
					'user': userId,
					'category': category,
					'fixed': null
				}

				if (payment_date) transaction.payment_date = payment_date;
				if (payment_date_limit) transaction.payment_date_limit = payment_date_limit;
				if (payment_type) transaction.payment_type = payment_type;

			    Transaction.create(transaction, function(err, transactionSaved) {
			    	if (err) {
			    		console.log("Error syncing transaction id: " + row.id + " Error" + err);
			    	}
				  	if (transactionSaved) {
				  		//console.log('Transaction synced.')
				  	}
			    });
			});

		});

		db.close();
	};
}