var passport = require( 'passport' );
var FacebookStrategy = require( 'passport-facebook' ).Strategy;

function setupAuth( User, app, Config ) {
  // High level serialize/de-serialize configuration for passport
  passport.serializeUser( function( user, done ) {
    done( null, user._id );
  });

  passport.deserializeUser( function( id, done ) {
    User.findOne( { _id: id } ).exec( done );
  });

  var facebookCallbackDomain = Config.DOMAIN_LOCAL;
  var facebookCliendId = Config.FACEBOOK_CLIENT_ID_TEST;
  var facebookCliendSecret = Config.FACEBOOK_CLIENT_SECRET_TEST;

  if ( process.env.MONGODB_URI ) {
    facebookCallbackDomain = Config.DOMAIN;
    facebookCliendId = Config.FACEBOOK_CLIENT_ID;
    facebookCliendSecret = Config.FACEBOOK_CLIENT_SECRET;
  }

  // Facebook-specific
  passport.use( new FacebookStrategy({
      clientID: facebookCliendId,
      clientSecret: facebookCliendSecret,
      callbackURL: 'http://' + facebookCallbackDomain + '/auth/facebook/callback',
      profileFields: ['id', 'emails', 'name']
    },
    function( accessToken, refreshToken, profile, done ) {
      if ( !profile.emails || !profile.emails.length ) {
        return done( 'No emails associated with this account!' );
      }
      User.findOneAndUpdate(
        { 'data.oauth': profile.id },
        {
          $set: {
            'profile.email': profile.emails[0].value,
            'profile.givenName': profile.name.givenName,
            'profile.familyName': profile.name.familyName,
            'profile.picture': 'http://graph.facebook.com/' + profile.id.toString() + '/picture?type=large'
          }
        },
        { 'new': true, upsert: true, runValidators: true },
        function( error, user ) {
          done( error, user );
        }
      );
    }
  ));

  // Express middlewares
  app.use( require( 'express-session' )({
    secret: 'asdjkJasd12g9234sadfwcGhasd',
    resave: true,
    saveUninitialized: true
  }));
  app.use( passport.initialize() );
  app.use( passport.session() );

  // Express routes for auth
  app.get( '/auth/facebook', passport.authenticate( 'facebook', { 
    scope: ['email'],
    callbackURL: '/auth/facebook/callback'
  }));

  app.get( '/auth/facebook/callback',
    passport.authenticate( 'facebook', { failureRedirect: '/fail' }),
    function( req, res ) {
      console.log( `Welcome, ${req.user.profile.givenName}` );
      res.redirect( '/index.html' );
    });

  app.get( '/auth/logout', function( req, res ) {
    req.logout();
    res.redirect( '/index.html' );
  });
}

module.exports = setupAuth;