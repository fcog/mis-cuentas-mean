var CronJob = require( 'cron' ).CronJob;
var winston = require( 'winston' );

module.exports = function( User, FixedTransaction, Transaction ) {

  //1st day of month
  //0 0 1 * * *
  new CronJob('0 0 1 * * *', function( User, FixedTransaction, Transaction ) {
    winston.info("Fixed transactions syncing started");
    
    var date = new Date();
    var dateStart = new Date(date.getFullYear(), date.getMonth(), 1); // 1st day of month
    var dateEnd = new Date(date.getFullYear(), date.getMonth() + 1, 0); // last day of month
    
    User.find({}, function (err, users) {
      users.forEach(function(user) {
        //console.log("Fixed Transactions of " + user.profile.givenName);
        FixedTransaction.find({ user: user.id }, function (err, fixedTransactions) {
          //console.log("Fixed Transactions found: " + fixedTransactions.length);
          fixedTransactions.forEach(function(fixedTransaction) {
            Transaction.findOne({ fixed: fixedTransaction.id, createdAt: { $gte: dateStart, $lte: dateEnd } }, function (err, transaction) {
              //console.log("Fixed Transaction to sync: " + fixedTransaction.id);
              if (transaction == null) {
                // Transaction not sync
                var payment_date_limit = new Date(date.getFullYear(), date.getMonth(), fixedTransaction.repetition.day);
                var newTransaction = {
                  fixed: fixedTransaction.id,
                  title: fixedTransaction.title,
                  category: {
                    name: fixedTransaction.category.name,
                    type: fixedTransaction.category.type,
                    user: fixedTransaction.category.user
                  },
                  type: fixedTransaction.type,
                  payment_amount: fixedTransaction.payment_amount,
                  order_date: payment_date_limit,
                  payment_date_limit: payment_date_limit,
                  user: fixedTransaction.user
                };
                Transaction.create(newTransaction, function(error, transactionSaved) {
                  if (transactionSaved) {
                     //winston.log("Fixed transaction synced: " + transactionSaved.id);
                  }
                  else {
                     winston.log("Error syncing Fixed transaction: " + error);
                  }
                });
              }
              // else {
              //   // Transaction already sync
              //   console.log("Fixed transaction already synced: " + fixedTransaction.id);
              // }
            });
          });
        });
      });
    });

    winston.info("Fixed transactions syncing ended");
  }, null, true, null);

  //1minute of 12pm
  //1 0 * * *
  new CronJob('01 00 * * *', function( Transaction ) {
    winston.info("Urgent transactions scanning started");

    var today = new Date();
    var futureDate = new Date();
    futureDate.setDate( futureDate.getDate() + 4 );

    // find unpaid transactions which payment date limit is less than 4 days old
    Transaction.find({ payment_type: null, payment_date_limit: { $gte : today, $lte: futureDate } }, function (err, transactions) {
      
      transactions.forEach(function(transaction) {
        winston.log("Urgent transaction found: " + transaction.id);

        Transaction.update( { _id: transaction.id }, { $set: { urgent: true } }, { multi: false }, function(error, transactionUpdated) {
          winston.log("Urgent transaction updated");
        });
      });  
    });

    winston.info("Urgent transactions scanning ended");
  }, null, true, null);      

};