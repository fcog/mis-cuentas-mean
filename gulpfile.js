var gulp = require('gulp');
var mocha = require('gulp-mocha');
var nodemon = require('gulp-nodemon');
var notify = require('gulp-notify');
var livereload = require('gulp-livereload');
 
// Task
gulp.task('serve', function() {
	// listen for changes
	livereload.listen();
	// configure nodemon
	nodemon({
		// the script to run the app
		script: 'server.js',
		ext: 'js'
	}).on('restart', function(){
		// when the app has restarted, run livereload.
		gulp.src('server.js')
			.pipe(livereload())
			.pipe(notify('Reloading page, please wait...'));
	})
})

gulp.task('test', function(){
	gulp.
		src('./test/testTransactionsApi.js').
		pipe(mocha()).
		on('error', function(err){
			this.emit('end');
		});
});

gulp.task('watch', function() {
  gulp.watch(['./*.js', './**/*.js'], ['test']);
});
