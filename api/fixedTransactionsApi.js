var bodyparser = require( 'body-parser' );
var express = require( 'express' );
var status = require( 'http-status' );

module.exports = function( FixedTransaction, Transaction, Category ) {
  var api = express.Router();

  api.use( bodyparser.json() );

  // Sync a user's' fixed transactions of current month
  api.get( '/fixed-transactions/sync', function(req, res) {
      if (!req.user) {
        return res.
          status(status.UNAUTHORIZED).
          json({ error: 'Not logged in' });
      }

      var date = new Date();
      var dateStart = new Date(date.getFullYear(), date.getMonth(), 1); // 1st day of month
      var dateEnd = new Date(date.getFullYear(), date.getMonth() + 1, 0); // last day of month

      FixedTransaction.find({ user: req.user._id }, function (err, fixedTransactions) {
        //console.log("Fixed Transactions found: " + fixedTransactions.length);
        fixedTransactions.forEach(function(fixedTransaction) {
          Transaction.findOne({ fixed: fixedTransaction.id, createdAt: { $gte: dateStart, $lte: dateEnd } }, function (err, transaction) {
            //console.log("Fixed Transaction to sync: " + fixedTransaction.id);
            if (transaction == null) {
              // Transaction not sync
              var payment_date_limit = new Date(date.getFullYear(), date.getMonth(), fixedTransaction.repetition.day);
              var newTransaction = {
                fixed: fixedTransaction.id,
                title: fixedTransaction.title,
                category: {
                  name: fixedTransaction.category.name,
                  type: fixedTransaction.category.type,
                  user: fixedTransaction.category.user
                },
                type: fixedTransaction.type,
                payment_amount: fixedTransaction.payment_amount,
                order_date: payment_date_limit,
                payment_date_limit: payment_date_limit,
                user: fixedTransaction.user
              };
              Transaction.create(newTransaction, function(error, transactionSaved) {
                // if (transactionSaved) {
                //   console.log("Fixed transaction synced: " + transactionSaved.id);
                // }
                // else {
                //   console.log("Error syncing Fixed transaction: " + error);
                // }
              });
            }
            // else {
            //   // Transaction already sync
            //   console.log("Fixed transaction already synced: " + fixedTransaction.id);
            // }
          });
        });
        res.json({ message: 'Sync ended' });
      });
  });

  // Load a single fixed transaction
  api.get('/fixed-transactions/:id', function(req, res) {
      if (!req.user) {
        return res.
          status(status.UNAUTHORIZED).
          json({ error: 'Not logged in' });
      }
      FixedTransaction.findOne({ _id: req.params.id, user: req.user._id }, function(error, fixedTransaction) {
        if (error) {
          return res.
            status(status.INTERNAL_SERVER_ERROR).
            json({ error: error.toString() });
        }
        if (!fixedTransaction) {
          return res.
            status(status.NOT_FOUND).
            json({ error: 'Not found' });
        }
        res.json({ fixedTransaction: fixedTransaction });
      });
  });

  // Load a user's fixed transactions
  api.get('/fixed-transactions', function(req, res) {
      if (!req.user) {
        return res.
          status(status.UNAUTHORIZED).
          json({ error: 'Not logged in' });
      }

      var query = FixedTransaction.find({ user: req.user._id });

      // order by given order an order direction
      // fixed-transactions?orderby=payment_date&orderdir=desc
      if (req.query.orderby) {
        var sort = req.query.orderby;

        // order direction
        if (req.query.orderdir && req.query.orderdir == 'desc') {
          sort = '-' + sort;
        }

        query = query.sort(sort);
      }

      query.exec(function(error, fixedTransactions) {
        if (error) {
          return res.
            status(status.INTERNAL_SERVER_ERROR).
            json({ error: error.toString() });
        }
        if (!fixedTransactions) {
          return res.
            status(status.NOT_FOUND).
            json({ error: 'Not found' });
        }
        res.json({ fixedTransactions: fixedTransactions });
      });
  });

  // Save a fixed transaction
  api.post('/fixed-transactions', function(req, res) {
      if (!req.user) {
        return res.
          status(status.UNAUTHORIZED).
          json({ error: 'Not logged in' });
      }

      try {
        var fixedTransaction = req.body.fixedTransaction;
        fixedTransaction.category.type = req.body.fixedTransaction.type;
        fixedTransaction.category.user = req.body.fixedTransaction.user;
      } catch(e) {
        return res.
          status(status.BAD_REQUEST).
          json({ error: 'No transaction specified!' + e });
      }

      FixedTransaction.create(fixedTransaction, function( error, fixedTransactionSaved ) {
        if (error) {
          return res.
            status(status.INTERNAL_SERVER_ERROR).
            json({ error: error.toString() });
        }
        // create category if that doesn't exist
        Category.findOrCreate(fixedTransaction.category, function( error, categorySaved ) {
          if (error) {
            return res.
              status(status.INTERNAL_SERVER_ERROR).
              json({ error: error.toString() });
          }
          return res.json({ fixedTransaction: fixedTransactionSaved });
        });
      });
  });

  // Update a user's' fixed transaction by id (the fixed transaction's type can't be updated)
  api.put( '/fixed-transactions/:id', function(req, res) {
      if (!req.user) {
        return res.
          status(status.UNAUTHORIZED).
          json({ error: 'Not logged in' });
      }

      try {
        var condition = { _id: req.params.id, user: req.user._id };
        var update = {
          $set: {
            title: req.body.fixedTransaction.title,
            category: { 
              name: req.body.fixedTransaction.category.name,
              type: req.body.fixedTransaction.type,
              user: req.body.fixedTransaction.user
            },
            payment_amount: req.body.fixedTransaction.payment_amount,
            repetition: {
              day: req.body.fixedTransaction.repetition.day,
              type: req.body.fixedTransaction.repetition.type
            }
          }
        }
      } catch(e) {
        return res.
          status(status.BAD_REQUEST).
          json({ error: 'No transaction specified!' });
      }

      var options = { new: true };

      FixedTransaction.findOneAndUpdate(condition, update, options, function( error, fixedTransactionUpdated ) {
        if (error) {
          return res.
            status(status.INTERNAL_SERVER_ERROR).
            json({ error: error.toString() });
        }
        // create category if that doesn't exist
        Category.findOrCreate({ name: req.body.fixedTransaction.category.name, user: req.user._id }, function(error, categorySaved, created) {
          return res.json({ fixedTransaction: fixedTransactionUpdated });
        });
      });
  });

  // Delete a user's' transaction
  api.delete( '/fixed-transactions/:id', function(req, res) {
      if (!req.user) {
        return res.
          status(status.UNAUTHORIZED).
          json({ error: 'Not logged in' });
      }

      try {
        var condition = { _id: req.params.id, user: req.user._id };
      } catch(e) {
        return res.
          status(status.BAD_REQUEST).
          json({ error: 'No transaction specified!' });
      }

      FixedTransaction.remove(condition, function(error) {
        if (error) {
          return res.
            status(status.INTERNAL_SERVER_ERROR).
            json({ error: error.toString() });
        }
        return res.json({ fixedTransaction: { id: req.params.id } });
      });
  });

  return api;
};
