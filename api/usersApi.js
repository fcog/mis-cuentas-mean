var bodyparser = require( 'body-parser' );
var express = require( 'express' );
var status = require( 'http-status' );

module.exports = function() {
    var api = express.Router();

    api.use( bodyparser.json() );

    // Returns the personal user's info
    api.get( '/me', function( req, res ) {
        if ( !req.user ) {
            return res.
                status( status.UNAUTHORIZED ).
                json( { error: 'Not logged in' } );
        }

        return res.json( { user: req.user } );
    });

    return api;
};
