var bodyparser = require( 'body-parser' );
var express = require( 'express' );
var status = require( 'http-status' );

module.exports = function( Category, Transaction ) {
    var api = express.Router();

    api.use( bodyparser.json() );

    // Load a user's categories
    api.get( '/categories', function(req, res) {
            if (!req.user) {
                return res.
                status(status.UNAUTHORIZED).
                json({ error: 'Not logged in' });
            }

            var query = Category.find({ user: req.user._id });

            // get transactions by type
            // categories?type=Expense
            if ( req.query.type ) {
                var type = req.query.type === 'expense' 
                ? 'Expense' : req.query.type === 'income' 
                ? 'Income' : req.query.type;
                query = query.where( 'type' ).equals( type );
            }

            // order by given order an order direction
            // categories?orderby=title&orderdir=desc
            if (req.query.orderby) {
                var sort = req.query.orderby;

                // order direction
                if (req.query.orderdir && req.query.orderdir == 'desc') {
                    sort = '-' + sort;
                }

                query = query.sort(sort);
            }

            query.exec(function(error, categories) {
                if (error) {
                    return res.
                    status(status.INTERNAL_SERVER_ERROR).
                    json({ error: error.toString() });
                }
                if (!categories) {
                    return res.
                    status(status.NOT_FOUND).
                    json({ error: 'Not found' });
                }

                res.json({ categories: categories });
            });
    });

    // get a user's category information
    api.get( '/category/:name', function( req, res ) {
            if ( ! req.user ) {
                return res.
                status( status.UNAUTHORIZED ).
                json( { error: 'Not logged in' } );
            }

            // get transactions by type.
            // Default Expense transactions
            // category?type=Expense
            var type = 'Expense';
            if ( req.query.type && req.query.type === 'income' ) {
                type = 'income';
            }

            // Calculate sum on given dates or else use current month
            if ( req.query.date_start && req.query.date_end ) {
                var dateStart = new Date( req.query.date_start );
                var dateEnd = new Date( req.query.date_end );
            } else {
                var date = new Date();
                var dateStart = new Date( date.getFullYear(), date.getMonth(), 1 ); // 1st day of month
                var dateEnd = new Date( date.getFullYear(), date.getMonth() + 1, 0 ); // last day of month           
            }
              
            var query = Transaction
                .aggregate(
                    // Limit to relevant documents
                    { $match: {
                        $and: [
                            { user: req.user._id },
                            { type: type },
                            { 'category.name': req.params.name },
                            { payment_date: { $gte: dateStart, $lte: dateEnd } },
                            { payment_amount: { $ne: null } }
                        ]
                    }},            
                    { $group: {
                        _id : null,
                        total: { $sum: '$payment_amount' }
                    }}
                );

            query.exec( function( error, data ) {
                if ( error ) {
                    return res.
                    status( status.INTERNAL_SERVER_ERROR ).
                    json( { error: error.toString() } );
                }
                if ( ! data ) {
                    return res.
                    status( status.NOT_FOUND).
                    json( { error: 'Not found' } );
                }

                res.json( { amount: data } );
            });
    });

    return api;
};
