var bodyparser = require( 'body-parser' );
var express = require( 'express' );
var status = require( 'http-status' );
// var api = require( 'express-promise-router' )();

module.exports = function( Transaction, Category ) {
	var api = express.Router();

	api.use( bodyparser.json() );

	// Get first and last transactions paid
	api.get( '/transactions/first-last', function( req, res ) {
		if ( !req.user ) {
			return res.
				status( status.UNAUTHORIZED ).
				json( { error: 'Not logged in' } );
		}

		Transaction.
			find( { user: req.user._id } ).
			sort( { "payment_date" : 1 } ).
			limit( 1 ).
			exec( function( error, transactionFirst ) {
				if ( error ) {
				return res.
				  status( status.INTERNAL_SERVER_ERROR ).
				  json( { error: error.toString() } );
				}

				if ( !transactionFirst ) {
					return res.
					  status( status.NOT_FOUND ).
					  json( { error: 'Not found' } );
				}

				Transaction.
					find( { user: req.user._id } ).
					sort( { "payment_date" : -1 } ).
					limit( 1 ).
					exec( function( error, transactionLast ) {
						if ( error ) {
							return res.
							status( status.INTERNAL_SERVER_ERROR ).
							json( { error: error.toString() } );
						}

						if ( !transactionLast ) {
							return res.
								status( status.NOT_FOUND ).
								json( { error: 'Not found' } );
						}

						res.json( { transactions: [transactionFirst, transactionLast] } );
					});
			});
	});

  	// Load a single transaction
	api.get( '/transactions/:id', function( req, res ) {
		if ( !req.user ) {
			return res.
				status( status.UNAUTHORIZED ).
				json( { error: 'Not logged in' } );
		}

	  	Transaction.findOne( { _id: req.params.id, user: req.user._id }, function( error, transaction ) {
			if (error) {
				return res.
					status( status.INTERNAL_SERVER_ERROR ).
					json( { error: error.toString() } );
			}

			if ( !transaction ) {
				return res.
					status( status.NOT_FOUND ).
					json( { error: 'Not found' } );
			}

			res.json( { transaction: transaction } );
		});
	});

	// Load a user's transactions
	api.get('/transactions', function(req, res) {
		if (!req.user) {
			return res.
				status(status.UNAUTHORIZED).
				json({ error: 'Not logged in' });
		}

		// transactions?urgent=true
		// get urgent transactions first
		var query1 = Transaction
			.find({ user: req.user._id })
			.where('urgent').equals(true)
			.sort( { urgent: -1 } );

		// get transactions by 2 given dates
		// transactions?date_start=2016-06-20&date_end=2016-06-30
		if (req.query.date_start && req.query.date_end) {
			query1 = query1.where('order_date').gte(req.query.date_start).lte(req.query.date_end);
		}
		// get transactions by 1 given dates
		// transactions?date_start=2016-06-20
		else if (req.query.date_start) {
			query1 = query1.where('order_date').gte(req.query.date_start);
		}

		query1.exec(function( error, urgentTransactions ) {
			if ( error ) {
				return res.
					status(status.INTERNAL_SERVER_ERROR).
					json({ error: error.toString() });
			}
			if ( !urgentTransactions ) {
				return res.
					status(status.NOT_FOUND).
					json({ error: 'Not found' });
			}

			if (req.query.urgent) {
				// return only the urgent transactions
				res.json( { transactions: urgentTransactions } );
			} else {
				// get rest of transactions second
				var query2 = Transaction
					.find( { user: req.user._id, $or: [ { urgent: { $exists: false } }, { urgent: false } ] } );

				// get transactions by 2 given dates
				// transactions?date_start=2016-06-20&date_end=2016-06-30
				if (req.query.date_start && req.query.date_end) {
			  		query2 = query2.where('order_date').gte(req.query.date_start).lte(req.query.date_end);
				}
				// get transactions by 1 given dates
				// transactions?date_start=2016-06-20
				else if (req.query.date_start) {
			  		query2 = query2.where('order_date').gte(req.query.date_start);
				}

				// order by given order an order direction
				// transactions?orderby=payment_date&orderdir=desc
				if (req.query.orderby) {
					var sortField = req.query.orderby;
					var sortDirection = -1;

					// order direction
					if (req.query.orderdir && req.query.orderdir == 'asc') {
						sortDirection = 1;
					}

					var sorting = {};

					sorting[sortField] = sortDirection;

					query2 = query2.sort( sorting );
				} 

				query2.exec(function(error, transactions) {
					if (error) {
						return res.
							status(status.INTERNAL_SERVER_ERROR).
							json({ error: error.toString() });
					}

					if (!transactions) {
						return res.
							status(status.NOT_FOUND).
							json({ error: 'Not found' });
					}

					res.json({ transactions: urgentTransactions.concat(transactions) });
				});
			}
		});
	});

  // Save a transaction
  api.post( '/transactions', function( req, res ) {
		if ( !req.user ) {
			return res.
				status( status.UNAUTHORIZED ).
				json( { error: 'Not logged in' } );
		}

		try {
			var transaction = req.body.transaction;
			transaction.order_date = req.body.transaction.payment_date;
			transaction.category.type = req.body.transaction.type;
			transaction.category.user = req.user;
			transaction.user = req.user;
		} catch( e ) {
			return res.
				status( status.BAD_REQUEST ).
				json( { error: 'No transaction specified!' + e } );
		}

		Transaction.create( transaction, function( error, transactionSaved ) {
			if (error) {
				return res.
					status(status.INTERNAL_SERVER_ERROR).
					json({ error: error.toString() });
			}

			// create category if that doesn't exist
			Category.findOrCreate( transaction.category, function( error, categorySaved ) {
				if (error) {
					return res.
						status(status.INTERNAL_SERVER_ERROR).
						json({ error: error.toString() });
				}
				return res.json({ transaction: transactionSaved });
			});
		});
	});

  // Update a transaction by id (the transaction's type can't be updated)
  api.put( '/transactions/:id', function( req, res ) {
		if ( !req.user ) {
			return res.
				status(status.UNAUTHORIZED).
				json({ error: 'Not logged in' });
		}

		try {
			var condition = { _id: req.params.id, user: req.user._id };
			var order_date = (req.body.transaction.payment_date) ? req.body.transaction.payment_date : req.body.transaction.payment_date_limit;
			var update = {
			  $set: {
				title: req.body.transaction.title,
				category: {
				  name: req.body.transaction.category.name
				},
				payment_amount: req.body.transaction.payment_amount,
				payment_type: req.body.transaction.payment_type,
				payment_date: req.body.transaction.payment_date,
				order_date: order_date,
				urgent: false,
				payment_date_limit: req.body.transaction.payment_date_limit
			  }
			}
		} catch( e ) {
			return res.
				status( status.BAD_REQUEST ).
				json( { error: 'No transaction specified!' } );
		}

		var options = { new: true };

		Transaction.findOneAndUpdate(condition, update, options, function(error, transactionUpdated) {
			if (error) {
				return res.
					status(status.INTERNAL_SERVER_ERROR).
					json({ error: error.toString() });
			}

			// create category if that doesn't exist
			var category = {
				name: req.body.transaction.category.name,
				type: req.body.transaction.type,
				user: req.user._id
			};

			Category.findOrCreate( category, function(error, categorySaved, created) {
				return res.json({ transaction: transactionUpdated });
			});
		});
	});

  // Delete a transaction
  api.delete('/transactions/:id', function(req, res) {
	  if (!req.user) {
		return res.
		  status(status.UNAUTHORIZED).
		  json({ error: 'Not logged in' });
	  }

	  try {
		var condition = { _id: req.params.id, user: req.user._id };
	  } catch(e) {
		return res.
		  status(status.BAD_REQUEST).
		  json({ error: 'No transaction specified!' });
	  }

	  Transaction.remove(condition, function(error) {
		if (error) {
		  return res.
			status(status.INTERNAL_SERVER_ERROR).
			json({ error: error.toString() });
		}
		return res.json({ transaction: { id: req.params.id } });
	  });
  	});

  return api;
};
