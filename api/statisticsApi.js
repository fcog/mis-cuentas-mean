var bodyparser = require( 'body-parser' );
var express = require( 'express' );
var status = require( 'http-status' );

module.exports = function( Transaction ) {
  var api = express.Router();

  api.use( bodyparser.json() );

  // Calculates the total sum of incomes and expenses grouped by month and year
  api.get( '/statistics/sum', function( req, res ) {
      if ( !req.user ) {
        return res.
          status( status.UNAUTHORIZED ).
          json( { error: 'Not logged in' } );
      }

      var type = 'Expense';
      if ( req.query.type && req.query.type == 'income' ) {
        type = 'Income';
      }

      if ( req.query.date_start && req.query.date_end ) {
          var dateStart = new Date( req.query.date_start );
          var dateEnd = new Date( req.query.date_end );
          
          var query = Transaction
            .aggregate(
                // Limit to relevant documents
                { $match: {
                    $and: [
                        { user: req.user._id },
                        { type: type },
                        { payment_date: { $gte: dateStart, $lte: dateEnd } },
                        { payment_amount: { $ne: null } }
                    ]
                }},            
                { $group: {
                    _id : null,
                    total: { $sum: '$payment_amount' }
                }}
            );
      } else {
        // return statistics grouped by month and year
        var query = Transaction
          .aggregate(
              // Limit to relevant documents and potentially take advantage of an index
              { $match: {
                  $and: [
                      { user: req.user._id },
                      { type: type },
                      { payment_amount: { $ne: null } },
                      { payment_date: { $ne: null } }
                  ]
              }},
              { $group: {
                  _id : { year: { $year : '$payment_date' }, month: { $month : '$payment_date' } },
                  debit: { $sum: { $cond: [ { $eq: [ '$payment_type', 'Debit' ] }, '$payment_amount', 0 ] } },
                  credit: { $sum: { $cond: [ { $eq: [ '$payment_type', 'Credit' ] }, '$payment_amount', 0 ] } }
              }}
          );
      }

      // order by date given an order direction
      // statistics/sum?orderdir=desc
      if ( req.query.orderdir ) {
        var sortDirection = -1;

        // order direction
        if ( req.query.orderdir == 'asc' ) {
            sortDirection = 1;
        }

        var sorting = { '_id.year': sortDirection, '_id.month': sortDirection };

        query = query.sort( sorting );
      } 

      query.exec( function( error, result ) {
          if ( error ) {
            return res.
              status( status.INTERNAL_SERVER_ERROR ).
              json( { error: error.toString() } );
          }
          
          if ( ! result ) {
            return res.
              status( status.NOT_FOUND ).
              json( { error: 'Error calculating statistics' } );
          }
          res.json( result );
      });
  });

  // Calculates the total sum grouped by category
  api.get( '/statistics/category', function(req, res) {
      if (!req.user) {
        return res.
          status(status.UNAUTHORIZED).
          json({ error: 'Not logged in' });
      }

      var type = 'Expense';
      if (req.query.type && req.query.type == 'income') {
        type = 'Income';
      }

      if ( req.query.date_start && req.query.date_end ) {
          var dateStart = new Date( req.query.date_start );
          var dateEnd = new Date( req.query.date_end );
          
          var query = Transaction
            .aggregate(
                // Limit to relevant documents
                { $match: {
                    $and: [
                        { user: req.user._id },
                        { type: type },
                        { payment_date: { $gte: dateStart, $lte: dateEnd } },
                        { payment_amount: { $ne: null } }
                    ]
                }},            
                { $group: {
                    _id : { category: '$category.name' },
                    total: { $sum: '$payment_amount' }
                }},
                { $sort: { 'categories.name': 1 }}
            );
      } else {
        // return statistics grouped by month and year
        var query = Transaction
          .aggregate(
              // Limit to relevant documents and potentially take advantage of an index
              { $match: {
                  $and: [
                      { user: req.user._id },
                      { type: type },
                      { payment_amount: { $ne: null } },
                      { payment_date: { $ne: null } }
                  ]
              }},
              { $group: {
                  _id : { 
                    year: { $year : '$payment_date' }, 
                    month: { $month : '$payment_date' }, 
                    category: '$category.name' 
                  },
                  sum: { 
                    $sum: '$payment_amount' 
                  }
              }},
              { $group: {
                  _id : { 
                    year: '$_id.year', 
                    month: '$_id.month' 
                  },
                  categories: { 
                    $push: { 
                      name: '$_id.category',
                      total: '$sum'
                    }
                  }
              }},
              { $unwind: '$categories' },
              { $sort: { 'categories.name': 1 }},
              { $group: {
                  _id : { 
                    year: '$_id.year', 
                    month: '$_id.month' 
                  },
                  categories: { 
                    $push: { 
                      name: '$categories.name',
                      total: '$categories.total'
                    }
                  }
              }}           
          );
      }

      // order by date given an order direction
      // statistics/sum?orderdir=desc
      if (req.query.orderdir) {
        var sortDirection = -1;

        // order direction
        if (req.query.orderdir == 'asc') {
            sortDirection = 1;
        }

        var sorting = { '_id.year': sortDirection, '_id.month': sortDirection };

        query = query.sort( sorting );
      } 

      query.exec(function(error, result) {
          if (error) {
            return res.
              status(status.INTERNAL_SERVER_ERROR).
              json({ error: error.toString() });
          }
          
          if (!result) {
            return res.
              status(status.NOT_FOUND).
              json({ error: 'Error calculating statistics' });
          }
          res.json(result);
      });
  });

  // Calculates the total sum by category
  api.get( '/statistics/category/average', function(req, res) {
      if (!req.user) {
        return res.
          status(status.UNAUTHORIZED).
          json({ error: 'Not logged in' });
      }

      var type = 'Expense';
      if (req.query.type && req.query.type == 'income') {
        type = 'Income';
      }

      if ( req.query.date_start && req.query.date_end ) {
          var dateStart = new Date( req.query.date_start );
          var dateEnd = new Date( req.query.date_end );
          
          var query = Transaction
            .aggregate(
                // Limit to relevant documents
                { $match: {
                    $and: [
                        { user: req.user._id },
                        { type: type },
                        { payment_date: { $gte: dateStart, $lte: dateEnd } },
                        { payment_amount: { $ne: null } }
                    ]
                }},            
                { $group: {
                    _id : { category: '$category.name' },
                    total: { $avg: '$payment_amount' }
                }},
                { $sort: { 'categories.name': 1 }}
            );
      } else {
        // return statistics grouped by month and year
        var query = Transaction
          .aggregate(
              // Limit to relevant documents and potentially take advantage of an index
              { $match: {
                  $and: [
                      { user: req.user._id },
                      { type: type },
                      { payment_amount: { $ne: null } },
                      { payment_date: { $ne: null } }
                  ]
              }},
              { $group: {
                  _id : { 
                    year: { $year : '$payment_date' }, 
                    month: { $month : '$payment_date' }, 
                    category: '$category.name' 
                  },
                  sum: { 
                    $sum: '$payment_amount' 
                  }
              }},
              { $group: {
                  _id : { 
                    category: '$_id.category'
                  },
                  average: {
                    $avg: '$sum'
                  }
              }}      
          );
      }

      // order by date given an order direction
      // statistics/sum?orderdir=desc
      if (req.query.orderdir) {
        var sortDirection = -1;

        // order direction
        if (req.query.orderdir == 'asc') {
            sortDirection = 1;
        }

        var sorting = { '_id.year': sortDirection, '_id.month': sortDirection };

        query = query.sort( sorting );
      } 

      query.exec(function(error, result) {
          if (error) {
            return res.
              status(status.INTERNAL_SERVER_ERROR).
              json({ error: error.toString() });
          }
          
          if (!result) {
            return res.
              status(status.NOT_FOUND).
              json({ error: 'Error calculating statistics' });
          }
          res.json(result);
      });
  });

  return api;
};